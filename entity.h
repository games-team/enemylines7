#ifndef __el__entity_h
#define __el__entity_h

#include <iostream>
#include <vector>

#include "coordinate.h"
#include "entitytype.h"
#include "tweak/tweak.h"
#include "position.h"

#include "release.h"

namespace PRJID {

class Container;
class Game;

class Entity {
	bool collision;
	bool collision_y;

	C3f dest;
	Entity * mdest;
	Container *container_;
	Game *game_;
	unsigned int exploding;

	void init();
	int ttl;
	float speed;

	bool m_forward,m_backward,m_left,m_right;
	unsigned int myticks;
	unsigned int jumping;
public:
	Position position;
	e_entitytype type;
	bool remove;

	Entity();


	C3f get_position() const;
	void set_position(C3f p);
	//C3f get_rotation() const;

	void set_container(Container *c) { container_=c; }
	void set_game(Game *g) { game_=g; }

	void draw();
	bool isactive();

	void set_type(e_entitytype type);
	

	void act(unsigned int ticks);
	void tick(unsigned int ticks);
	bool trymove(C3f d);

	void hit();
	void explode();
	void drop_bomb();

	void move_direction(C3f r,float mod=1.0f);
	void move_forward(float mod=1.0f);
	void move_backward(float mod=1.0f);
	void move_left(float mod=1.0f);
	void move_right(float mod=1.0f);


	void mark_move_forward();
	void mark_move_backward();
	void mark_move_left();
	void mark_move_right();

	void turn_up();
	void turn_down();
	void turn_left();
	void turn_right();
	void turn(int x,int y);

	void jump();

	Entity *clone();
};


} //namespace

#endif
