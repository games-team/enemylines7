#ifndef __el__quaternion_h
#define __el__quaternion_h

#include "SDL_opengl.h"

#include "mymath.h"

class Quaternion  {
private:
	float x,y,z,w;
public:
	Quaternion();
	virtual ~Quaternion();

	void to_matrix(float *);
	void from_axisangle(C3f axis,float angle);



	Quaternion operator *(Quaternion q);
};


#endif
