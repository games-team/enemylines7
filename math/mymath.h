#ifndef ___mymath
#define ___mymath
#include "c3_tpl.h"

typedef C3_tpl <int> C3;
typedef C3_tpl <int> C3i;
typedef C3_tpl <unsigned int> C3ui;
typedef C3_tpl <float> C3f;
typedef C3_tpl <double> C3d;
typedef C3_tpl <bool> C3b;

#include "box3_tpl.h"

#include "c4_tpl.h"
#include "matrix4_tpl.h"


#include "frustum.h"

#include "quaternion.h"

typedef Box3_tpl <float> Box3f;
#endif
