#ifndef __el__skybox_h
#define __el__skybox_h

#include <iostream>
#include "release.h"

namespace PRJID {


class Skybox {

public:

	static void draw(int seed=0);
	static void inner_draw(int seed=0);
	static void gen_dl();

};


} //namespace

#endif
