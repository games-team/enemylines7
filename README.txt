enemy lines 7 v0.5 by Raphael Pikrin
pi_raph at yahoo com
http://raum1.memebot.com/enemylines/
http://proj.phk.at/el/

use "make" or "scons" to build
./enemylines7 to run

use -help for a list of commandline options

make install  copies to /usr/local/bin/ and /usr/local/share/enemylines7/


AUDIO:

if audio lags behind you might want to change the SDL_AUDIODRIVER environment
variable. e.g.: export  SDL_AUDIODRIVER=dsp
http://www.libsdl.org/cgi/docwiki.cgi/SDL_5fenvvars

