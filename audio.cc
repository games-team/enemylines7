
#include <vector>
#include <list>
#include "audio.h"
#include "SDL.h"

#ifndef NOAUDIO
#include "SDL_mixer.h"
#endif


namespace PRJID {


bool on_=true;

bool Audio::init() {
	#ifndef NOAUDIO
	on_=false;
	const int buffer=512;
	if( SDL_InitSubSystem( SDL_INIT_AUDIO) < 0 ) {
		std::cout << "Error SDL_InitSubSystem( SDL_INIT_AUDIO) " << SDL_GetError() << std::endl;
		return false;
	}
   if (Mix_OpenAudio( MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, buffer) < 0) {
   	if (Mix_OpenAudio( MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, buffer) < 0) {
			std::cout << "Error : " << Mix_GetError() << std::endl;
			return false;
		}
   } 
	Mix_AllocateChannels(AC_LAST);
	Mix_ReserveChannels(AC_LASER);
	on_=true;
	#endif
	return true;
}

void Audio::off() {on_=false; }
void Audio::on() {on_=true; }
void Audio::toggle() {on_=!on_; }



#ifndef NOAUDIO
Mix_Chunk *loadchunk(std::string file) {
	return Mix_LoadWAV(file.c_str());
}


std::vector <Mix_Chunk *> chunks;
#endif

bool Audio::load(std::string path) {
	bool err=false;
	#ifndef NOAUDIO
	Mix_Chunk *c;

	chunks.resize(AS_LAST);
	std::string name;

	std::string ext=".ogg";

	name=path+"failed"+ext;
	c=loadchunk(name);
	if (c==NULL) {std::cout << "failed to load " << name << std::endl;err=true; }
	chunks[AS_FAILED]=c;

	name=path+"projectile"+ext;
	c=loadchunk(name);
	if (c==NULL) { std::cout << "failed to load " << name<<std::endl; err=true; }
	chunks[AS_LASER]=c;

	name=path+"explosion_plane"+ext;
	c=loadchunk(name);
	if (c==NULL) { std::cout << "failed to load " << name<<std::endl; err=true; }
	chunks[AS_EXPLOSION_PLANE]=c;

	name=path+"explosion_building"+ext;
	c=loadchunk(name);
	if (c==NULL) { std::cout << "failed to load " << name<<std::endl; err=true; }
	chunks[AS_EXPLOSION_BUILDING]=c;

	name=path+"prob1"+ext;
	c=loadchunk(name);
	if (c==NULL) {std::cout << "failed to load " << name << std::endl;err=true; }
	chunks[AS_PROBLEM]=c;

	name=path+"jetpack2"+ext;
	c=loadchunk(name);
	if (c==NULL) { std::cout << "failed to load " << name<<std::endl; err=true; }
	chunks[AS_JETPACK]=c;

	#endif
	
	return !err;
}
void Audio::play(E_AudioSample s,E_AudioChannel chan,int repeat) {
	#ifndef NOAUDIO
	if (!on_) return;
	if (chunks.size()==0) return;
	Mix_Chunk *c;
	c=chunks[s];
	if (!c) return;

	Mix_PlayChannel(chan, c, repeat);
	#endif
}


class toplay {
public:
	toplay() {}
	toplay(unsigned int delay,E_AudioSample s,E_AudioChannel c,int r) {
		sample=s;
		chan=c;
		repeat=r;
		tick=SDL_GetTicks()+delay;
	}
	unsigned int tick;
	E_AudioSample sample;
	E_AudioChannel chan;
	int repeat;

	bool check() {
		if (tick==0) return false;
		if (SDL_GetTicks()<tick) return true;
		Audio::play(sample,chan,repeat);
		tick=0;
		return false;
	}
};

std::list <toplay> schedule;
void Audio::tick() {
	if (!on_) return;

	bool keep=false;
	std::list <toplay>::iterator it;
	for (it=schedule.begin();it!=schedule.end();it++) {
		if ((*it).check()) {
			keep=true;
		}
	}
	if (!keep) schedule.resize(0);

}


void Audio::schedule_play(unsigned int delay,E_AudioSample s,E_AudioChannel chan,int repeat) {
	toplay t=toplay(delay,s,chan,repeat);
	schedule.push_back(t);
}

} //namespace
