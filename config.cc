

#include "config.h"

namespace PRJID {


bool mouse_reverse_=false;


int Config::mouse_reverse() {
	if (mouse_reverse_) return -1;
	return 1;
}

void Config::toggle_mouse_reverse() {
	mouse_reverse_=!mouse_reverse_;
}

unsigned int dx_=800;
unsigned int dy_=600;
void Config::resolution(unsigned int x,unsigned int y) {
	dx_=x;
	dy_=y;
}

unsigned int Config::dx() { return dx_; }
unsigned int Config::dy() { return dy_; }

unsigned int Config::cx() { return dx_/2; }
unsigned int Config::cy() { return dy_/2; }

unsigned int Config::tx() { return dx_/3; }
unsigned int Config::ty() { return dy_/3; }


bool record_=false;

bool Config::record(){
	return record_;
}
void Config::toggle_record() {
	record_=!record_;
}


bool show_gun_=true;

bool Config::show_gun() {
	return show_gun_;
}
void Config::toggle_show_gun() {
	show_gun_=!show_gun_;
}

} //namespace
