#include "SDL_opengl.h"
#include "bunker.h"


#include <iostream>

namespace models {

static GLuint bunker_dl=0;

void bunker::draw() {
	dldraw();
}
void bunker::dldraw() {
	if (bunker_dl==0) { sdraw(); return; }
	glCallList(bunker_dl);
}

void bunker::gen_dl() {
	bunker_dl=glGenLists(1);
	glNewList(bunker_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded bunker " << bunker_dl << std::endl;
}
namespace mtl_bunker {
typedef enum MTL {
m_concrete,
m_default,
m_door,
m_last
};
} //namespace
void bunker::material (int id) {
	switch (id) {
		case mtl_bunker::m_concrete:
static const GLfloat m_concrete_diffuse[] = {0.600400,0.600400,0.600400};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_concrete_diffuse);
static const GLfloat m_concrete_ambient[] = {0.613720,0.613720,0.613720};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_concrete_ambient);
static const GLfloat m_concrete_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_concrete_emissive);
			break;
		case mtl_bunker::m_default:
static const GLfloat m_default_diffuse[] = {1.00000,1.00000,1.00000};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_default_diffuse);
static const GLfloat m_default_ambient[] = {1.00000,1.00000,1.00000};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_default_ambient);
static const GLfloat m_default_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_default_emissive);
			break;
		case mtl_bunker::m_door:
static const GLfloat m_door_diffuse[] = {0.693640,0.693640,0.693640};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_door_diffuse);
static const GLfloat m_door_ambient[] = {0.813520,0.813520,0.813520};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_door_ambient);
static const GLfloat m_door_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_door_emissive);
			break;
	}
}
float bunker::minx() { return -8.14271 ; }
float bunker::miny() { return 0 ; }
float bunker::minz() { return -7.55448 ; }
float bunker::maxx() { return 8.81595; }
float bunker::maxy() { return 2.50107; }
float bunker::maxz() { return 9.40419; }
float bunker::radius() { return 13.1307; }
void bunker::sdraw() {
const float vertices[22][3]={
	{-0.40522598,0.0000000e+0,9.40418614},
	{-0.16041699,1.90672597,6.60600663},
	{6.01777177,1.90672597,1.42189072},
	{8.81595128,0.0000000e+0,1.66669971},
	{-8.14271241,0.0000000e+0,0.18300889},
	{-5.34453290,1.90672597,0.42781787},
	{0.83365586,1.90672597,-4.75629804},
	{1.07846485,0.0000000e+0,-7.55447754},
	{5.1320527e-2,1.60792086,4.18583574},
	{3.59760087,1.60792086,1.21015320},
	{0.62191834,1.60792086,-2.33612714},
	{-2.92436200,1.60792086,0.63955539},
	{0.20709373,2.50106806,2.40533987},
	{1.81710501,2.50106806,1.05438000},
	{0.46614514,2.50106806,-0.55563128},
	{-1.14386614,2.50106806,0.79532859},
	{4.18371273,1.66457177,-1.15627703},
	{5.34980738,0.18934511,-1.69374920},
	{3.78597141,0.18934511,-3.55745634},
	{3.05417444,1.66457177,-2.50240834},
	{6.23678913,0.0000000e+0,-1.40702604},
	{3.65762699,0.0000000e+0,-4.48075179},
};
const float normals[22][3]={
	{-7.9416912e-2,0.41194907,0.90773945},
	{-1.8396255e-2,0.97747022,0.21027016},
	{0.29659503,0.94984282,-9.9146432e-2},
	{0.68334387,0.72277526,-0.10313622},
	{-0.90773945,0.41194907,-7.9416912e-2},
	{-0.21027016,0.97747022,-1.8396255e-2},
	{0.14914336,0.94984282,-0.27487249},
	{0.22023077,0.72277526,-0.65505292},
	{-1.3705656e-2,0.98755807,0.15665637},
	{0.15665637,0.98755807,1.3705656e-2},
	{1.3705656e-2,0.98755807,-0.15665637},
	{-0.15665637,0.98755807,-1.3705656e-2},
	{-2.5802918e-2,0.95517081,0.29492870},
	{0.29492870,0.95517081,2.5802918e-2},
	{2.5802918e-2,0.95517081,-0.29492870},
	{-0.29492870,0.95517081,-2.5802918e-2},
	{0.55427352,0.68223318,-0.47680053},
	{0.43203570,0.82176040,-0.37155753},
	{0.44093494,0.82176040,-0.36095182},
	{0.56580545,0.68223318,-0.46305732},
	{0.48675275,0.77217445,-0.40843405},
	{0.48675275,0.77217445,-0.40843405},
};
//o cube2
material(mtl_bunker::m_concrete);
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[21]);
	glVertex3fv(vertices[21]);
	glNormal3fv(normals[20]);
	glVertex3fv(vertices[20]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[20]);
	glVertex3fv(vertices[20]);
	glNormal3fv(normals[21]);
	glVertex3fv(vertices[21]);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
	glNormal3fv(normals[21]);
	glVertex3fv(vertices[21]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[20]);
	glVertex3fv(vertices[20]);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
material(mtl_bunker::m_door);
glBegin(GL_POLYGON);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
glEnd();
}
} //namespace
