#include "SDL_opengl.h"
#include "slope4_2.h"


#include <iostream>

namespace models {

static GLuint slope4_2_dl=0;

void slope4_2::draw() {
	dldraw();
}
void slope4_2::dldraw() {
	if (slope4_2_dl==0) { sdraw(); return; }
	glCallList(slope4_2_dl);
}

void slope4_2::gen_dl() {
	slope4_2_dl=glGenLists(1);
	glNewList(slope4_2_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded slope4_2 " << slope4_2_dl << std::endl;
}
float slope4_2::minx() { return -6.12303e-17 ; }
float slope4_2::miny() { return 0 ; }
float slope4_2::minz() { return 0 ; }
float slope4_2::maxx() { return 1; }
float slope4_2::maxy() { return 1; }
float slope4_2::maxz() { return 1; }
float slope4_2::radius() { return 1.73205; }
void slope4_2::sdraw() {
const float vertices[11][3]={
	{1.00000000,0.0000000e+0,1.00000000},
	{1.00000000,1.00000000,1.00000000},
	{1.00000000,1.00000000,1.1102230e-16},
	{1.00000000,0.0000000e+0,1.1102230e-16},
	{-6.1230318e-17,0.0000000e+0,1.00000000},
	{-6.1230318e-17,0.22403556,1.00000000},
	{0.0000000e+0,1.00000000,0.0000000e+0},
	{0.0000000e+0,0.0000000e+0,0.0000000e+0},
	{0.76510222,0.13984000,1.00000000},
	{-3.0615159e-17,0.38787556,0.50000000},
	{0.75000000,0.87484444,0.50000000},
};
const float normals[11][3]={
	{0.33333333,-0.66666667,0.66666667},
	{-0.18548947,0.62379476,0.75925869},
	{3.7062229e-3,0.99266564,-0.12083542},
	{0.57735027,-0.57735027,-0.57735027},
	{-0.57735027,-0.57735027,0.57735027},
	{-0.48510903,0.51171197,0.70909808},
	{-0.83015208,0.52516187,-0.18722324},
	{-0.66666667,-0.66666667,-0.33333333},
	{-0.27930772,0.42445507,0.86129268},
	{-0.62766635,0.66359622,0.40703195},
	{-0.63109561,0.70964651,0.31324138},
};
//o cube2
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
}
} //namespace
