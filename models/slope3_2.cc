#include "SDL_opengl.h"
#include "slope3_2.h"


#include <iostream>

namespace models {

static GLuint slope3_2_dl=0;

void slope3_2::draw() {
	dldraw();
}
void slope3_2::dldraw() {
	if (slope3_2_dl==0) { sdraw(); return; }
	glCallList(slope3_2_dl);
}

void slope3_2::gen_dl() {
	slope3_2_dl=glGenLists(1);
	glNewList(slope3_2_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded slope3_2 " << slope3_2_dl << std::endl;
}
float slope3_2::minx() { return 0 ; }
float slope3_2::miny() { return 0 ; }
float slope3_2::minz() { return 0 ; }
float slope3_2::maxx() { return 1; }
float slope3_2::maxy() { return 1; }
float slope3_2::maxz() { return 1; }
float slope3_2::radius() { return 1.73205; }
void slope3_2::sdraw() {
const float vertices[10][3]={
	{0.0000000e+0,0.0000000e+0,1.00000000},
	{0.0000000e+0,1.00000000,1.00000000},
	{1.00000000,1.00000000,1.00000000},
	{1.00000000,0.0000000e+0,1.00000000},
	{0.0000000e+0,0.0000000e+0,0.0000000e+0},
	{0.0000000e+0,0.22403556,0.0000000e+0},
	{1.00000000,1.00000000,0.0000000e+0},
	{1.00000000,0.0000000e+0,0.0000000e+0},
	{0.0000000e+0,1.00000000,0.50000000},
	{0.50000000,0.38787556,0.0000000e+0},
};
const float normals[10][3]={
	{-0.66666667,-0.66666667,0.33333333},
	{-0.57735027,0.57735027,0.57735027},
	{0.21409605,0.96339690,0.16133598},
	{0.57735027,-0.57735027,0.57735027},
	{-0.57735027,-0.57735027,-0.57735027},
	{-0.52511264,0.23839044,-0.81696188},
	{0.18722324,0.52516187,-0.83015208},
	{0.33333333,-0.66666667,-0.66666667},
	{-0.57221302,0.69399449,-0.43697128},
	{-0.16288571,0.47891590,-0.86261684},
};
//o cube2
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
}
} //namespace
