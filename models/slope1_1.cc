#include "SDL_opengl.h"
#include "slope1_1.h"


#include <iostream>

namespace models {

static GLuint slope1_1_dl=0;

void slope1_1::draw() {
	dldraw();
}
void slope1_1::dldraw() {
	if (slope1_1_dl==0) { sdraw(); return; }
	glCallList(slope1_1_dl);
}

void slope1_1::gen_dl() {
	slope1_1_dl=glGenLists(1);
	glNewList(slope1_1_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded slope1_1 " << slope1_1_dl << std::endl;
}
float slope1_1::minx() { return 0 ; }
float slope1_1::miny() { return 0 ; }
float slope1_1::minz() { return 0 ; }
float slope1_1::maxx() { return 1; }
float slope1_1::maxy() { return 1; }
float slope1_1::maxz() { return 1; }
float slope1_1::radius() { return 1.73205; }
void slope1_1::sdraw() {
const float vertices[9][3]={
	{0.0000000e+0,0.0000000e+0,1.00000000},
	{0.0000000e+0,1.00000000,1.00000000},
	{0.60177778,0.66044444,0.29066667},
	{1.00000000,0.0000000e+0,1.00000000},
	{0.0000000e+0,0.0000000e+0,0.0000000e+0},
	{0.0000000e+0,1.00000000,0.0000000e+0},
	{1.00000000,1.00000000,0.0000000e+0},
	{1.00000000,0.0000000e+0,0.0000000e+0},
	{0.43777778,0.75688889,0.64533333},
};
const float normals[9][3]={
	{-0.20397044,-0.31692665,0.92625782},
	{0.29677671,0.87687632,0.37816862},
	{0.62455030,0.70315811,0.33986114},
	{0.79533974,0.28665570,0.53410037},
	{-0.57735027,-0.57735027,-0.57735027},
	{-0.31357188,0.93793231,-0.14817441},
	{0.56407509,0.62218469,0.54286785},
	{0.94090321,-0.31471596,-0.12512001},
	{0.56946725,0.59784156,0.56417420},
};
//o cube1
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
}
} //namespace
