
#include "all.h"

#include <iostream>

namespace models {


void gen_dl() {
	models::sphere::gen_dl();
	models::plane1::gen_dl();
	models::plane2::gen_dl();
	models::bomb::gen_dl();
	models::slope1_1::gen_dl();
	models::slope1_2::gen_dl();
	models::slope2_1::gen_dl();
	models::slope2_2::gen_dl();
	models::slope3_1::gen_dl();
	models::slope3_2::gen_dl();
	models::slope4_1::gen_dl();
	models::slope4_2::gen_dl();
	models::tower::gen_dl();
	models::biosphere::gen_dl();
	models::station::gen_dl();
	models::bunker::gen_dl();
}

}
