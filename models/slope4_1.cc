#include "SDL_opengl.h"
#include "slope4_1.h"


#include <iostream>

namespace models {

static GLuint slope4_1_dl=0;

void slope4_1::draw() {
	dldraw();
}
void slope4_1::dldraw() {
	if (slope4_1_dl==0) { sdraw(); return; }
	glCallList(slope4_1_dl);
}

void slope4_1::gen_dl() {
	slope4_1_dl=glGenLists(1);
	glNewList(slope4_1_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded slope4_1 " << slope4_1_dl << std::endl;
}
float slope4_1::minx() { return -6.12303e-17 ; }
float slope4_1::miny() { return 0 ; }
float slope4_1::minz() { return 0 ; }
float slope4_1::maxx() { return 1; }
float slope4_1::maxy() { return 1; }
float slope4_1::maxz() { return 1; }
float slope4_1::radius() { return 1.73205; }
void slope4_1::sdraw() {
const float vertices[10][3]={
	{1.00000000,0.0000000e+0,1.00000000},
	{1.00000000,1.00000000,1.00000000},
	{1.00000000,1.00000000,1.1102230e-16},
	{1.00000000,0.0000000e+0,1.1102230e-16},
	{-6.1230318e-17,0.0000000e+0,1.00000000},
	{-6.1230318e-17,0.22403556,1.00000000},
	{0.0000000e+0,1.00000000,0.0000000e+0},
	{0.0000000e+0,0.0000000e+0,0.0000000e+0},
	{0.50000000,0.13984000,1.00000000},
	{-3.0615159e-17,0.38787556,0.50000000},
};
const float normals[10][3]={
	{0.33333333,-0.66666667,0.66666667},
	{0.12015161,0.44578283,0.88704073},
	{-2.8675611e-2,0.98276055,0.18264556},
	{0.57735027,-0.57735027,-0.57735027},
	{-0.57735027,-0.57735027,0.57735027},
	{-0.46357558,0.51662517,0.71985840},
	{-0.83015208,0.52516187,-0.18722324},
	{-0.66666667,-0.66666667,-0.33333333},
	{-0.23887440,0.59317777,0.76881672},
	{-0.57565984,0.66815625,0.47136289},
};
//o cube2
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
}
} //namespace
