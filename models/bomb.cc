#include "SDL_opengl.h"
#include "bomb.h"


#include <iostream>

namespace models {

static GLuint bomb_dl=0;

void bomb::draw() {
	dldraw();
}
void bomb::dldraw() {
	if (bomb_dl==0) { sdraw(); return; }
	glCallList(bomb_dl);
}

void bomb::gen_dl() {
	bomb_dl=glGenLists(1);
	glNewList(bomb_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded bomb " << bomb_dl << std::endl;
}
namespace mtl_bomb {
typedef enum MTL {
m_hull,
m_last
};
} //namespace
void bomb::material (int id) {
	switch (id) {
		case mtl_bomb::m_hull:
static const GLfloat m_hull_diffuse[] = {0.640360,0.640360,0.640360};
			glMaterialfv(GL_FRONT,GL_DIFFUSE, m_hull_diffuse);
static const GLfloat m_hull_ambient[] = {0.653680,0.653680,0.653680};
			glMaterialfv(GL_FRONT,GL_AMBIENT, m_hull_ambient);
static const GLfloat m_hull_emissive[] = {0.00000e+0,0.00000e+0,0.00000e+0};
			glMaterialfv(GL_FRONT,GL_EMISSION, m_hull_emissive);
			break;
	}
}
float bomb::minx() { return -0.216 ; }
float bomb::miny() { return -0.440227 ; }
float bomb::minz() { return -0.216 ; }
float bomb::maxx() { return 0.216; }
float bomb::maxy() { return 0.440227; }
float bomb::maxz() { return 0.216; }
float bomb::radius() { return 0.535829; }
void bomb::sdraw() {
const float vertices[24][3]={
	{-0.17193600,-0.23842275,0.17193600},
	{-0.21600000,0.44022749,0.21600000},
	{0.21600000,0.44022749,0.21600000},
	{0.17193600,-0.23842275,0.17193600},
	{-0.17193600,-0.23842275,-0.17193600},
	{-0.21600000,0.44022749,-0.21600000},
	{0.21600000,0.44022749,-0.21600000},
	{0.17193600,-0.23842275,-0.17193600},
	{-0.17193600,0.19487698,0.17193600},
	{-3.8016000e-2,0.33090542,3.8016000e-2},
	{-0.21600000,0.39490927,0.21600000},
	{0.21600000,0.39490927,0.21600000},
	{3.8016000e-2,0.33090542,3.8016000e-2},
	{0.17193600,0.19487698,0.17193600},
	{-0.17193600,0.19487698,-0.17193600},
	{-3.8016000e-2,0.33090542,-3.8016000e-2},
	{-0.21600000,0.39490927,-0.21600000},
	{0.21600000,0.39490927,-0.21600000},
	{3.8016000e-2,0.33090542,-3.8016000e-2},
	{0.17193600,0.19487698,-0.17193600},
	{7.0493760e-2,-0.44022749,7.0493760e-2},
	{-7.0493760e-2,-0.44022749,7.0493760e-2},
	{-7.0493760e-2,-0.44022749,-7.0493760e-2},
	{7.0493760e-2,-0.44022749,-7.0493760e-2},
};
const float normals[24][3]={
	{-0.67039411,-0.31803063,0.67039411},
	{-0.57735027,0.57735027,0.57735027},
	{0.57735027,0.57735027,0.57735027},
	{0.67039411,-0.31803063,0.67039411},
	{-0.67039411,-0.31803063,-0.67039411},
	{-0.57735027,0.57735027,-0.57735027},
	{0.57735027,0.57735027,-0.57735027},
	{0.67039411,-0.31803063,-0.67039411},
	{-0.61184817,0.50128198,0.61184817},
	{-0.67303580,-0.30666857,0.67303580},
	{-0.50142286,-0.70508881,0.50142286},
	{0.50142286,-0.70508881,0.50142286},
	{0.67303580,-0.30666857,0.67303580},
	{0.61184817,0.50128198,0.61184817},
	{-0.61184817,0.50128198,-0.61184817},
	{-0.67303580,-0.30666857,-0.67303580},
	{-0.50142286,-0.70508881,-0.50142286},
	{0.50142286,-0.70508881,-0.50142286},
	{0.67303580,-0.30666857,-0.67303580},
	{0.61184817,0.50128198,-0.61184817},
	{0.39181493,-0.83244347,0.39181493},
	{-0.39181493,-0.83244347,0.39181493},
	{-0.39181493,-0.83244347,-0.39181493},
	{0.39181493,-0.83244347,-0.39181493},
};
//o cube1
material(mtl_bomb::m_hull);
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[21]);
	glVertex3fv(vertices[21]);
	glNormal3fv(normals[20]);
	glVertex3fv(vertices[20]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[20]);
	glVertex3fv(vertices[20]);
	glNormal3fv(normals[23]);
	glVertex3fv(vertices[23]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[22]);
	glVertex3fv(vertices[22]);
	glNormal3fv(normals[21]);
	glVertex3fv(vertices[21]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[23]);
	glVertex3fv(vertices[23]);
	glNormal3fv(normals[22]);
	glVertex3fv(vertices[22]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[13]);
	glVertex3fv(vertices[13]);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
	glNormal3fv(normals[19]);
	glVertex3fv(vertices[19]);
	glNormal3fv(normals[14]);
	glVertex3fv(vertices[14]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[16]);
	glVertex3fv(vertices[16]);
	glNormal3fv(normals[17]);
	glVertex3fv(vertices[17]);
	glNormal3fv(normals[18]);
	glVertex3fv(vertices[18]);
	glNormal3fv(normals[15]);
	glVertex3fv(vertices[15]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[21]);
	glVertex3fv(vertices[21]);
	glNormal3fv(normals[22]);
	glVertex3fv(vertices[22]);
	glNormal3fv(normals[23]);
	glVertex3fv(vertices[23]);
	glNormal3fv(normals[20]);
	glVertex3fv(vertices[20]);
glEnd();
}
} //namespace
