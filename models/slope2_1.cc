#include "SDL_opengl.h"
#include "slope2_1.h"


#include <iostream>

namespace models {

static GLuint slope2_1_dl=0;

void slope2_1::draw() {
	dldraw();
}
void slope2_1::dldraw() {
	if (slope2_1_dl==0) { sdraw(); return; }
	glCallList(slope2_1_dl);
}

void slope2_1::gen_dl() {
	slope2_1_dl=glGenLists(1);
	glNewList(slope2_1_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded slope2_1 " << slope2_1_dl << std::endl;
}
float slope2_1::minx() { return -6.1149e-17 ; }
float slope2_1::miny() { return 0 ; }
float slope2_1::minz() { return 0 ; }
float slope2_1::maxx() { return 1; }
float slope2_1::maxy() { return 1; }
float slope2_1::maxz() { return 1; }
float slope2_1::radius() { return 1.73205; }
void slope2_1::sdraw() {
const float vertices[8][3]={
	{1.00000000,0.0000000e+0,1.00000000},
	{1.00000000,1.00000000,1.00000000},
	{0.40621292,0.63600255,0.61237137},
	{1.00000000,0.0000000e+0,1.1102230e-16},
	{-6.1149003e-17,0.0000000e+0,1.00000000},
	{-6.1149003e-17,1.00000000,1.00000000},
	{0.0000000e+0,1.00000000,0.0000000e+0},
	{0.0000000e+0,0.0000000e+0,0.0000000e+0},
};
const float normals[8][3]={
	{0.70398734,-0.50219609,0.50219609},
	{0.66559346,0.74012918,9.5886128e-2},
	{0.56269041,0.60801312,-0.56008888},
	{0.70808146,-5.7575022e-2,-0.70377963},
	{-0.57735027,-0.57735027,0.57735027},
	{-0.50901413,0.69412479,0.50901413},
	{-9.3936407e-2,0.74229284,-0.66345859},
	{-0.50193568,-0.50193568,-0.70435868},
};
//o cube1
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
}
} //namespace
