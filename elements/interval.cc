
#include "interval.h"

namespace PRJID {


Interval::Interval(unsigned int l) {
	length=l;
	last=0;
	ticks=0;
}

Interval::~Interval() {
}


void Interval::tick(unsigned int t) {
	ticks+=t;

}

bool Interval::check() {
	if (ticks>last+length) {
		last+=length;
		return true;
	}
	return false;
}


std::ostream& operator<<(std::ostream&s, Interval sk) {
	return s;
}

} //namespace
