#include <sstream>


#include "difficulty.h"

#include "../font_ogl.h"

namespace PRJID {

	Difficulty::Difficulty() {
		label_dl=0;
		reset();
	}

	Difficulty::~Difficulty() {
		if (label_dl!=0) { glDeleteLists(label_dl,1); }
	}


	void Difficulty::reset() {
		current=D_EASY;
	}

	void Difficulty::set_current(E_Difficulty d) {
		current=d;
	}

	float Difficulty::linear_higher_with_diffi(float min,float max) {
		float r;
		r=static_cast<float>(current);
		r/=D_NIGHTMARE;
		return r;
	}

	float Difficulty::linear_lower_with_diffi(float min,float max) {
		float r;
		r=static_cast<float>(current);
		r/=D_NIGHTMARE;
		r=1.0f-r;

		float area;
		area=max-min;
		r*=area;
		r+=min;
		return r;
	}

	int Difficulty::linear_lower_with_diffi_i(int min,int max) {
		int r;
		r=static_cast<int>(linear_lower_with_diffi(
			static_cast<float>(min),
			static_cast<float>(max)
		));
		return r;
	}


	void Difficulty::label(std::string l,C3 p) {
	}

	void Difficulty::dim(C3 p,unsigned int w,unsigned int f) {
		pos=p;
	}

	void Difficulty::draw() {
	}

std::ostream& operator<<(std::ostream&s, Difficulty sk) {
	return s;
}

} //namespace
