#ifndef __el__difficulty_h
#define __el__difficulty_h

#include <iostream>

#include "../release.h"
#include "../coordinate.h"

#include "SDL_opengl.h"

namespace PRJID {

typedef enum E_Difficulty {
	D_NONE=0,
   D_EASY=1,
   D_MEDIUM=4,
   D_HARD=7,
   D_NIGHTMARE=10,
	D_LAST=11
};


class Difficulty {
	E_Difficulty current;
	C3 pos;
	GLuint label_dl;
public:
	Difficulty();
	~Difficulty();
	void reset();
	
	void set_current(E_Difficulty d);
	E_Difficulty get() { return current; }


	float linear_higher_with_diffi(float min=0,float max=1);
	float linear_lower_with_diffi(float min=0,float max=1);

	int linear_lower_with_diffi_i(int min=0,int max=10);


	void label(std::string n,C3 pos);
	void dim(C3 pos,unsigned int width,unsigned int font=0);
	void draw();
};

std::ostream& operator<<(std::ostream&s, Difficulty);

} //namespace

#endif
