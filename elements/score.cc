#include <sstream>


#include "score.h"

#include "../font_ogl.h"

namespace PRJID {

	Score::Score() {
		label_dl=0;
		reset();
	}

	Score::~Score() {
		if (label_dl!=0) { glDeleteLists(label_dl,1); }
	}


	void Score::reset() {
		current=0;
	}


	void Score::set_current(unsigned int c) {
		current=c;
	}
	void Score::gain(unsigned int g) {
		current+=g;
	}
	void Score::reduce(unsigned int g) {
		if (g>=current) { 
			current=0;
		} else {
			current-=g;
		}
	}
	void Score::change(int g) {
		if (g>0) gain(g);
		if (g<0) reduce(g*-1);
	}

	void Score::label(std::string l,C3 p) {
			label_dl=glGenLists(1);
			glNewList(label_dl,GL_COMPILE);
			glColor3ub(250,250,250);
			Font_ogl::write(p,l);
			glEndList();
	}

	void Score::dim(C3 p,unsigned int w,unsigned int f) {
		pos=p;
		width=w;
		font=f;
	}

	void Score::draw() {
		std::ostringstream sstr;
		sstr.str("");
		sstr.setf ( std::ios_base::right, std::ios_base::basefield );
		sstr.setf(std::ios_base::showbase);
		sstr.width ( width );
		sstr << current;
		glColor3ub(250,250,250);
		Font_ogl::write(pos,sstr.str());

		if (label_dl!=0) {
			glCallList(label_dl);
		}

	}

std::ostream& operator<<(std::ostream&s, Score sk) {
	return s;
}

} //namespace
