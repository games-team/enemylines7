
#include "SDL_opengl.h"

#include "floor.h"

#include "coordinate.h"

#include "tweak/tweak.h"
#include "models/all.h"

namespace PRJID {

GLuint floor_dl=0;

void Floor::draw() {
	if (floor_dl==0) {
		inner_draw();
		return;
	}
	glCallList(floor_dl);
}
void Floor::gen_dl() {
   floor_dl=glGenLists(1);
   glNewList(floor_dl,GL_COMPILE);
   inner_draw();
   glEndList();
   std::cout << " loaded floor " << floor_dl << std::endl;
}

void Floor::inner_draw() {
	C3f p;


	glPushMatrix();
	p=Tweak::floor_tower_pos_C3f();
	glTranslatef( p.x,p.y,p.z );
	models::tower::dldraw();
	glPopMatrix();

	glPushMatrix();
	p=Tweak::floor_biosphere_pos_C3f();
	glTranslatef( p.x,p.y,p.z );
	models::biosphere::dldraw();
	glPopMatrix();

	glPushMatrix();
	p=Tweak::floor_station_pos_C3f();
	glTranslatef( p.x,p.y,p.z );
	models::station::dldraw();
	glPopMatrix();

	glPushMatrix();
	p=Tweak::floor_bunker_pos_C3f();
	glTranslatef( p.x,p.y,p.z );
	models::bunker::dldraw();
	glPopMatrix();

	glEnable(GL_COLOR_MATERIAL);
	glColor3f(.1,.1,.1);
	glNormal3f(0,1,0);
   glBegin(GL_QUADS);
   glVertex3i(0,0,0);
   glVertex3i(0,0,80);
   glVertex3i(80,0,80);
   glVertex3i(80,0,0);
   glEnd();
	glColor3f(.1,.2,.1);
	float f=1000;
   glBegin(GL_QUADS);
   glVertex3f(-f,-.1,-f);
   glVertex3f(-f,-.1,f);
   glVertex3f(f,-.1,f);
   glVertex3f(f,-.1,-f);
   glEnd();
}


} //namespace
