#include "SDL.h"
#include "SDL_opengl.h"
#include <iostream>

#include "font_data.h"
#include "font_ogl.h"

static GLuint dls_smooth,dls;


Font_data font;


unsigned int Font_ogl::dx() { return font.dx(); }
unsigned int Font_ogl::dy() { return font.dy(); }

char Font_ogl::get_field(int letter,int x,int y) {
	return font.get_field(letter,x,y);
}


void Font_ogl::drawletter(char letter,bool smooth) {
	if (smooth) {
		glCallList(dls_smooth+letter-33);
	} else {
		glCallList(dls+letter-33);
	}
}

void Font_ogl::drawletter_nodl(char letter,bool smooth) {

	SDL_Rect p;
	for (unsigned int l=0;l<dy();l++) {
		p.y=l;
		for (unsigned int c=0;c<dx();c++) {
			p.x=c;
			glPushMatrix();
			glTranslatef(p.x,p.y,0.0f);
			char chr;
			chr=get_field(letter,c,l);

			float d=0.5f;
			float n=0;
			float o=0;
			if (chr==' ') {
				if (!smooth) { glPopMatrix(); continue;}
				if (get_field(letter,c-1,l)!=' ') {
					n=d;
					o=1.0f-d;
					if (get_field(letter,c,l+1)!=' ') {
						glBegin(GL_TRIANGLES);
						glVertex2f(0,n);
						glVertex2f(o,1);
						glVertex2f(0,1);
						glEnd();
					}

					if (get_field(letter,c,l-1)!=' ') {
						glBegin(GL_TRIANGLES);
						glVertex2f(0,0);
						glVertex2f(o,0);
						glVertex2f(0,o);
						glEnd();
					}

				}
				if (get_field(letter,c+1,l)!=' ') {
					if (get_field(letter,c,l-1)!=' ') {
						glBegin(GL_TRIANGLES);
						glVertex2f(n,0);
						glVertex2f(1,0);
						glVertex2f(1,o);
						glEnd();
					}
					if (get_field(letter,c,l+1)!=' ') {
						glBegin(GL_TRIANGLES);
						glVertex2f(1,n);
						glVertex2f(1,1);
						glVertex2f(o,1);
						glEnd();
					}

				}

				glPopMatrix();
				continue;
			}
			glBegin(GL_QUADS);
			glVertex2f(0.0f,0.0f);
			glVertex2f(0.0f,1.0f);
			glVertex2f(1.0f,1.0f);
			glVertex2f(1.0f,0.0f);
			glEnd();
			glPopMatrix();
		}
	}
}


void Font_ogl::genlist() {

	dls=glGenLists(94);
	for (int i=33;i<127;i++) {
		int li;
		li=dls+i-33;
		glNewList(dls+i-33,GL_COMPILE);
		drawletter_nodl((char)i,false);
		glEndList();
	}
	dls_smooth=glGenLists(94);
	for (int i=33;i<127;i++) {
		int li;
		li=dls+i-33;
		glNewList(dls_smooth+i-33,GL_COMPILE);
		drawletter_nodl((char)i,true);
		glEndList();
	}
}
void Font_ogl::write(const std::string str,bool smooth,E_FontAnchor a) {
	write(str.c_str(),smooth,a);
}
void Font_ogl::write_nodl(const std::string str,bool smooth,E_FontAnchor a) {
	write_nodl(str.c_str(),smooth,a);
}

void Font_ogl::write(const char *text,bool smooth,E_FontAnchor a) {
	glPushMatrix();
	if (a==FA_CENTER) {
		glTranslatef(-(float)(strlen(text)*dx()/2),-(float)(dy()/2),0);
	}
	SDL_Rect p;
	for (unsigned int i=0;i<strlen(text);i++) {
		char letter=text[i];
		if (letter==' ') continue;
		if (letter<33||letter>126) continue;

		glPushMatrix();
		p.x=i*dx();
		glTranslatef(p.x,0,0);
		drawletter(letter,smooth);
		glPopMatrix();
	}
	glPopMatrix();
}

void Font_ogl::write_nodl(const char *text,bool smooth,E_FontAnchor a) {
	SDL_Rect p;
	for (unsigned int i=0;i<strlen(text);i++) {
		char letter=text[i];

		glPushMatrix();
		p.x=i*dx();
		glTranslatef(p.x,0,0);
		drawletter_nodl(letter,smooth);
		glPopMatrix();
	}
}
void Font_ogl::write(C3 pos,const char *text,bool smooth,E_FontAnchor a) {
	glPushMatrix();
	glLoadIdentity();
	glTranslatef(pos.x,pos.y,pos.z);
	write(text,smooth,a);
	glPopMatrix();
}
void Font_ogl::write(C3 pos,const std::string str,bool smooth,E_FontAnchor a) {
	glPushMatrix();
	glLoadIdentity();
	glTranslatef(pos.x,pos.y,pos.z);
	write(str,smooth,a);
	glPopMatrix();
}
