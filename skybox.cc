
#include "SDL_opengl.h"

#include "skybox.h"
#include "coordinate.h"
#include "tweak/tweak.h"

namespace PRJID {

GLuint dl=0;

void Skybox::draw(int seed) {
	if (dl==0) {
		inner_draw();
		return;
	}
	glCallList(dl);
}

void Skybox::gen_dl() {
   dl=glGenLists(1);
   glNewList(dl,GL_COMPILE);
   inner_draw();
   glEndList();
   std::cout << " loaded skybox " << dl << std::endl;
}

void Skybox::inner_draw(int seed) {
   glPushMatrix();
   float size=Tweak::skybox_size_f();
   glTranslatef(-size,-size,-size);
   glScalef(size*2,size*2,size*2);

	C3 colors[8]= { C3(), C3(), C3(), C3(), C3(), C3(), C3(), C3()};


static const float normals[6][3] = {
	{1,0,0},
	{-1,0,0},
	{0,1,0},
	{0,-1,0},
	{0,0,1},
	{0,0,-1}
};

static const float vertices[8][3] = {
   {1.0f,1.0f,1.0f},
   {1.0f,1.0f,0.0f},
   {0.0f,1.0f,0.0f},
   {0.0f,1.0f,1.0f},
   {1.0f,0.0f,1.0f},
   {1.0f,0.0f,0.0f},
   {0.0f,0.0f,1.0f},
   {0.0f,0.0f,0.0f}
};
static const unsigned int faceindices[6][4] = {
   {8,3,4,7},
   {6,5,1,2},
   {6,8,7,5},
   {1,4,3,2},
   {2,3,8,6},
   {5,7,4,1}
};

	colors[0]=Tweak::skybox1_color();
	colors[1]=Tweak::skybox2_color();
	colors[2]=Tweak::skybox3_color();
	colors[3]=Tweak::skybox4_color();
	colors[4]=Tweak::skybox5_color();
	colors[5]=Tweak::skybox6_color();
	colors[6]=Tweak::skybox7_color();
	colors[7]=Tweak::skybox8_color();


	glDisable(GL_LIGHTING);

	glColor3ub(100,30,200);
   glBegin(GL_QUADS);

	for (unsigned int i=0;i<6;i++) {

		glNormal3fv(normals[i]);
		C3 col;
		for (unsigned int v=0;v<4;v++) {
			col=colors[faceindices[i][v]-1];
			glColor3ub(col.x,col.y,col.z);
			glVertex3fv(vertices[faceindices[i][v]-1]);
		}



	}
   glEnd();

	glEnable(GL_LIGHTING);
   glPopMatrix();
}


} //namespace
