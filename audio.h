#ifndef __el__audio_h
#define __el__audio_h

#include <iostream>
#include "release.h"

namespace PRJID {


typedef enum E_AudioSample {
	AS_NONE,
	AS_TITLE,
	AS_FAILED,
	AS_EXPLOSION_BUILDING,
	AS_EXPLOSION_PLANE,
	AS_LASER,
	AS_JETPACK,
	AS_PROBLEM,
	AS_LAST
};

typedef enum E_AudioChannel {
	AC_NEXT=-1,
	AC_TITLE,
	AC_PROBLEM,
	AC_LASER,
	AC_EXPLOSION,
	AC_EXPLOSION2,
	AC_EXPLOSION3,
	AC_EXPLOSION4,
	AC_EXPLOSION5,
	AC_LAST
};

class Audio{
public:
	static bool init();
	static bool load(std::string dir);

	static void off();
	static void on();
	static void toggle();
	static void tick();

	static void play(E_AudioSample s,E_AudioChannel chan=AC_NEXT,int repeat=0);

	static void schedule_play(unsigned int delay,E_AudioSample s,E_AudioChannel chan=AC_NEXT,int repeat=0);
};

std::ostream& operator<<(std::ostream&s, Audio);

} //namespace

#endif
