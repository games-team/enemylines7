

#include "formation.h"
#include "entity.h"

#include "random.h"

namespace PRJID {

Entity * clone(Entity *e) {
	return e->clone();
}

void rv(Entity *e,int dir) {
	Entity *n;

	float dist=Tweak::formation_dist_f();
	float xd[]={dist*-2,dist*-1,dist*1,dist*2};
	float zd[]={dist*2,dist,dist,dist*2};

	int i;
	C3f pos;
	for (i=0;i<4;i++) {
		n=clone(e);
		pos=n->position.get_position();
		if (dir==1) {
			pos.x+=xd[i];
			pos.z+=zd[i];
		} else {
			pos.z+=xd[i];
			pos.x+=zd[i];
		}
		n->position.set_position(pos);
	}
}

void v(Entity *e,int dir) {
	Entity *n;

	float dist=Tweak::formation_dist_f();
	float xd[]={dist*-2,dist*-1,dist*1,dist*2};
	float zd[]={dist*2,dist,dist,dist*2};

	int i;
	C3f pos;
	for (i=0;i<4;i++) {
		n=clone(e);

		pos=n->position.get_position();
		//pos.x+=xd[i];
		//pos.z-=zd[i];
		if (dir==1) {
			pos.x+=xd[i];
			pos.z+=zd[i];
		} else {
			pos.z+=xd[i];
			pos.x+=zd[i];
		}
		n->position.set_position(pos);
	}
}
void line(Entity *e,int dir) {
	Entity *n;

	float dist=Tweak::formation_dist_f()*1.5;
	float xd[]={dist*-2,dist*-1,dist*1,dist*2};

	int i;
	C3f pos;
	for (i=0;i<4;i++) {
		n=clone(e);

		pos=n->position.get_position();
		//pos.x+=xd[i];
		if (dir==1) {
			pos.x+=xd[i];
		} else {
			pos.z+=xd[i];
		}
		n->position.set_position(pos);
	}
}

void parallel(Entity *e,int dir) {
	Entity *n;

	float dist=Tweak::formation_dist_f()*1.5;
	float xd[]={0,dist};
	float zd[]={0,dist,dist*2};

	C3f pos;
	int x,z;

	for (x=0;x<2;x++) {
		for (z=0;z<3;z++) {
			if (x==0&&z==0) continue;
			n=clone(e);

			pos=n->position.get_position();
			if (dir==1) {
				pos.x+=xd[x];
				pos.z+=zd[z];
			} else {
				pos.z+=xd[x];
				pos.x+=zd[z];
			}
			n->position.set_position(pos);


		}
	}


}


void Formation::apply(Entity *e) {
	switch (Random::sget(4)) {
		case 0: v(e,1); break;
		case 1: rv(e,1); break;
		case 2: line(e,1); break;
		case 3: parallel(e,1); break;
	}
}

void Formation::apply2(Entity *e) {
	switch (Random::sget(4)) {
		case 0: v(e,2); break;
		case 1: rv(e,2); break;
		case 2: line(e,2); break;
		case 3: parallel(e,2); break;
	}
}

} //namespace
