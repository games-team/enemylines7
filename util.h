

#include "coordinate.h"
#include "release.h"

namespace PRJID {

void error();

float destdegree(C3f p1,C3f p2);
C3f unproject(int x, int y);

void ortho2d(float dx=640,float dy=480);
void ortho2d_off();

void screenshot(int w,int hi,unsigned int t=0);


void bar(C3 start,C3 size);
void crosshair();

void menu_bg();


void lockmouse();
void unlockmouse();
void togglemouselock();
bool ismouselocked();

void took_on();
void took_off();

} //namespace
