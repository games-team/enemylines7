#ifndef __block__infostack_h
#define __block__infostack_h

#include <iostream>
#include <vector>

#include "blockinfo.h"

namespace block {

class Block;

class Infostack {
	std::vector < Blockinfo3 > infos;
public:

	Blockinfo3 top();

	bool empty();
	void push(Blockinfo3 b);
	void pop();

	Block *get_current();

};

std::ostream& operator<<(std::ostream&s, Infostack);

} //namespace

#endif
