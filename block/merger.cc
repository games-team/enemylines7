

#include "merger.h"

#include "block.h"

namespace block {

void Merger::merge(Block *b) {
	std::cout << " merge " << b << std::endl;
	Blockinfo3 bi;
	bi.block_=b;
	merge(bi);
}

void Merger::merge(Blockinfo3 bi) {
	if (bi.block_->is_splitted()) {
		for (unsigned int i=0;i<8;i++) {
			merge(bi.get_descend(i));
		}
		bi.block_->check_merge();
	}
}


} //namespace
