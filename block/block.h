#ifndef __el__block_h
#define __el__block_h

#include <iostream>
#include <vector>
#include <fstream>

#include "SDL_opengl.h"

#include "material.h"

namespace block {


class Block {
	std::vector <Block> sub;
	E_Material material;
public:
	GLuint displaylist;	

	Block();

	bool is_splitted() const;
	bool is_active() const;

	void activate();
	void deactivate();
	void toggle();

	void set_material(E_Material m);
	E_Material get_material() const;

	Block *get_child(unsigned int b);

	void dirty();

	void split();
	void merge();
	void check_merge();


	bool top_save(std::string filename) const;
	bool top_load(std::string filename);
	void save(std::ostream& st) const;
	void load(std::istream& st);

	static C3 sub_position(unsigned int i);
	static C3f sub_positionf(unsigned int i);
};


} //namespace

#endif
