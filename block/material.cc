

#include "material.h"

#include "coordinate.h"
#include "SDL_opengl.h"

#include "tweak/tweak.h"
#include "cube.h"
#include "models/all.h"

namespace block {


void draw_select(C3f pos,float scale) {
	glPushMatrix();
	glTranslatef(pos.x,pos.y,pos.z);	
	glScalef(scale,scale,scale);
	glScalef(1.1,1.1,1.1);
	glTranslatef(-.05,-.05,-.05);
	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	glLineWidth(3);
	glColor3f(1,1,1);
	Cube::draw();
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	glPopMatrix();
}

void draw_material(E_Material mat,int side) {
		float sc=0.1; float tr=0.4;
		C3 color;
		switch (mat) {
			case M_NONE: return;
			case M_SELECTED: 
				glColor3f(1,1,1);
				Cube::draw(side);
				return;
			case M_NONEEDIT:
         	glScalef(sc,sc,sc); glTranslatef(tr,tr,tr);
				glColor3f(.2,.3,.4);
				Cube::draw(side);
				return;
			case M_NONESELECTED:
         	glScalef(sc,sc,sc); glTranslatef(tr,tr,tr);
				glColor3f(1,1,1);
				Cube::draw(side);
				return;

			case M_DEFAULT:
				Cube::draw_colored(side);
				return;
			case M_WALL1:

				color=Tweak::material_wall1_color();
				glColor3ub(color.x,color.y,color.z);
				Cube::draw_wacky(side);
				return;
			case M_WALL2:
				color=Tweak::material_wall2_color();
				glColor3ub(color.x,color.y,color.z);
				Cube::draw(side);
				return;
			case M_WALL3:
				color=Tweak::material_wall3_color();
				glColor3ub(color.x,color.y,color.z);
				Cube::draw(side);
				return;
			case M_WALL4:
				color=Tweak::material_wall4_color();
				glColor3ub(color.x,color.y,color.z);
				Cube::draw(side);
				return;
			case M_WINDOW1:
				color=Tweak::material_window1_color();
				glColor3ub(color.x,color.y,color.z);
				Cube::draw(side);
				return;
			case M_WINDOW2:
				color=Tweak::material_window2_color();
				glColor3ub(color.x,color.y,color.z);
				Cube::draw(side);
				return;
			case M_STREET1:
				color=Tweak::material_street1_color();
				glColor3ub(color.x,color.y,color.z);
				Cube::draw_wacky(side);
				return;
			case M_BOMBEDWALL1:
				color=Tweak::material_bombedwall1_color();
				glColor3ub(color.x,color.y,color.z);
				Cube::draw(side);
				return;

			case M_SLOPE_BOMBED1_1:
				color=Tweak::material_slope_bombed1_color();
				glColor3ub(color.x,color.y,color.z);
				models::slope1_1::dldraw();
				return;
			case M_SLOPE_BOMBED1_2:
				color=Tweak::material_slope_bombed1_color();
				glColor3ub(color.x,color.y,color.z);
				models::slope1_2::dldraw();
				return;
			case M_SLOPE_BOMBED2_1:
				color=Tweak::material_slope_bombed1_color();
				glColor3ub(color.x,color.y,color.z);
				models::slope2_1::dldraw();
				return;
			case M_SLOPE_BOMBED2_2:
				glShadeModel(GL_FLAT);
				color=Tweak::material_slope_bombed1_color();
				glColor3ub(color.x,color.y,color.z);
				models::slope2_2::dldraw();
				glShadeModel(GL_SMOOTH);
				return;


			case M_SLOPE_BOMBED3_1:
				color=Tweak::material_slope_bombed1_color();
				glColor3ub(color.x,color.y,color.z);
				models::slope3_1::dldraw();
				return;
			case M_SLOPE_BOMBED3_2:
				color=Tweak::material_slope_bombed1_color();
				glColor3ub(color.x,color.y,color.z);
				models::slope3_2::dldraw();
				return;

			case M_SLOPE_BOMBED4_1:
				color=Tweak::material_slope_bombed1_color();
				glColor3ub(color.x,color.y,color.z);
				models::slope4_1::dldraw();
				return;
			case M_SLOPE_BOMBED4_2:
				color=Tweak::material_slope_bombed1_color();
				glColor3ub(color.x,color.y,color.z);
				models::slope4_2::dldraw();
				return;
			default:
				return;

		}
}


} //namespace
