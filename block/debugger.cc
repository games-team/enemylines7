

#include "debugger.h"

#include "material.h"
#include "block.h"
#include "tweak/tweak.h"
#include "cube.h"

namespace block {

void Debugger::debug(Block *b,Block *selected,bool showall) {
	std::cout << " debug " << b << "  " << selected << std::endl;
	Blockinfo3 bi;
	bi.block_=b;
	debug(bi,selected,showall);
}
void Debugger::debug(Blockinfo3 bi,Block *selected,bool showall) {
	if (bi.block_->is_splitted()) {
		if (bi.block_==selected) {
		}
		for (unsigned int i=0;i<8;i++) {
			debug(bi.get_descend(i),selected,showall);
		}
		return;
	}
	E_Material mat;

	mat=bi.block_->get_material();
	if (mat==M_NONE) {
		if (!showall) return;
		mat=M_NONEEDIT;
	}
	if (bi.block_==selected) {
	}
	std::cout << bi.block_ << "  " << selected << std::endl;
}


} //namespace
