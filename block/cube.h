#ifndef __PRJID__cube_h
#define __PRJID__cube_h

#include <iostream>

namespace block {


namespace Cube {


	void left();
	void right();
	void top();
	void bottom();
	void front();
	void back();

	void draw(int side=-1);
	void draw_wacky(int side=-1);
	void draw_colored(int side=-1);

	void gen_dl();


}


} //namespace

#endif
