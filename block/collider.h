#ifndef __block__collider_h
#define __block__collider_h

#include <iostream>

#include "coordinate.h"

#include "painter3.h"

namespace block {

class Block;

class Collider {
public:
	static bool collide(Block *b,C3f p,float s);
	static bool collide(Blockinfo3 b,C3f p,float s);
};


} //namespace

#endif
