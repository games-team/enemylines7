#ifndef __block__blockinfo_h
#define __block__blockinfo_h

#include <iostream>

#include "coordinate.h"

namespace block {

class Block;

class Blockinfo3 {
public:
	Blockinfo3();
	Block *block_;
	C3f pos_;
	unsigned int level_;
	float scale_;


	void reset_pos() { pos_=C3f(-1,-1,-1); }
	C3f get_pos() const { return pos_; }
	Block *get_current() const { return block_; }
	
	void descend(unsigned int i);
	Blockinfo3 get_descend(unsigned int i)const ;
};


} //namespace

#endif
