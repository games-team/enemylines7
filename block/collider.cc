

#include "collider.h"

#include "material.h"
#include "block.h"
#include "tweak/tweak.h"
#include "cube.h"

namespace block {



bool Collider::collide(Block *b,C3f testpos,float size) {
	Blockinfo3 bi;
	bi.block_=b;
	return collide(bi,testpos,size);
}
bool test(Blockinfo3 bi,C3f collisionpos,float size) {
	// create bbox for block
	C3f pos;
	Box3f box;
	pos=bi.pos_*Tweak::blockscale_f();
	pos-=size;
	box.add(pos);

	pos=bi.pos_;
	pos+=1*bi.scale_;
	pos*=Tweak::blockscale_f();
	pos+=size;
	box.add(pos);

	if (box.iswithin(collisionpos)) return true;
	return false;

}

bool Collider::collide(Blockinfo3 bi,C3f testpos,float size) {
	if (bi.block_->is_splitted()) {
		if (!test(bi,testpos,size)) {
			return false;
		}
		for (unsigned int i=0;i<8;i++) {
			if (collide(bi.get_descend(i),testpos,size)) return true;
		}
		return false;
	}
	E_Material mat;

	mat=bi.block_->get_material();
	if (mat==M_NONE) {
		return false;
	}
	if (!test(bi,testpos,size)) {
		return false;
	}
	return true;
}


} //namespace
