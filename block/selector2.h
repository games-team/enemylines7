#ifndef __block__selector2_h
#define __block__selector2_h

#include <iostream>

#include "coordinate.h"

#include "blockinfo.h"

#include "infostack.h"

namespace block {

class Block;


class Selector2 {
	unsigned int x,y,depth;
	Infostack selected;
	int level;
public:

	void select(Block *b,unsigned int nx,unsigned int ny,int level=-1);
	void select(Infostack st);
	Infostack get_result() { return selected; }
};


} //namespace

#endif
