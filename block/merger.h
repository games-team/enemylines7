#ifndef __block__merger_h
#define __block__merger_h

#include <iostream>

#include "coordinate.h"

#include "blockinfo.h"

namespace block {

class Block;

class Merger {

public:

	static void merge(Block *b);
	static void merge(Blockinfo3 b);
};


} //namespace

#endif
