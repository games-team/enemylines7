

#include "selector2.h"

#include "material.h"
#include "block.h"
#include "tweak/tweak.h"

namespace block {

void Selector2::select(Block *b,unsigned int nx,unsigned int ny,int l) {
	Infostack st;
	Blockinfo3 bi;
	bi.block_=b;
	st.push(bi);

	x=nx;
	y=ny;
	depth=std::numeric_limits<unsigned int>::max();
	level=l;


	select(st);
}

void Selector2::select(Infostack st) {
	if (st.top().get_current()->is_splitted()) {
		for (unsigned int i=0;i<8;i++) {
			st.push(st.top().get_descend(i));
			select(st);
			st.pop();
		}
		return;
	}
	E_Material mat;
	Blockinfo3 bi;
	bi=st.top();

	mat=bi.get_current()->get_material();
	if (mat==M_NONE) {
		if (Tweak::draw_all_i()!=1) return;
		mat=M_NONEEDIT;
	}

	glPushMatrix();
	glTranslatef(bi.pos_.x,bi.pos_.y,bi.pos_.z);	
	glScalef(bi.scale_,bi.scale_,bi.scale_);
	draw_material(mat);
	glPopMatrix();

	unsigned int p;
	glReadPixels(
		x,y,1,1,
		GL_DEPTH_COMPONENT, GL_UNSIGNED_INT,(GLvoid *)&p
	);

	if (p>=depth) return ;
	depth=p;
	selected=st;
	return ;

}


} //namespace
