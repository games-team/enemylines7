
DESTDIR	?= /usr/local

TARGET = enemylines7

CXX= g++

CXXFLAGS = -O2 -Wall `sdl-config --cflags` -I./

LDFLAGS = `sdl-config --libs` -lGL -lGLU -lSDL_mixer -lSDL_image


all: $(TARGET)

OFILES = audio.o config.o container.o entity.o random.o game.o util.o menu.o help.o font_ogl.o skybox.o math/frustum.o math/quaternion.o font_data.o position.o light.o formation.o tex.o models/sphere.o models/bomb.o models/displaylists.o models/plane1.o models/slope1_1.o models/slope1_2.o models/slope2_1.o models/slope2_2.o models/slope3_1.o models/slope3_2.o models/slope4_1.o models/slope4_2.o models/tower.o models/bunker.o models/biosphere.o models/plane2.o models/station.o floor.o radio.o main.o elements/energy.o elements/difficulty.o elements/interval.o elements/score.o elements/timeleft.o tweak/tweak_release.o block/block.o block/blockinfo.o block/cacher.o block/collider.o block/cube.o block/debugger.o block/destructor.o block/infostack.o block/map2.o block/material.o block/merger.o block/painter3.o block/painter6.o block/selector2.o

$(TARGET): $(OFILES)
	$(CXX) -o $(TARGET) $(OFILES) $(LDFLAGS)


clean:
	rm -f *.o models/*.o block/*.o tweak/*.o math/*.o elements/*.o $(TARGET)

install: 
	mkdir -p $(DESTDIR)/share/enemylines7/
	mkdir -p $(DESTDIR)/bin/
	cp enemylines7 $(DESTDIR)/bin/
	cp data/* $(DESTDIR)/share/enemylines7/
