#ifndef __el__entitytype_h
#define __el__entitytype_h

#include "release.h"

namespace PRJID {

typedef enum e_entitytype {
	ET_NONE,
	ET_PLAYER,
	ET_BOMB,
	ET_FLOATING,
	ET_PROJECTILE,
	ET_PLANE1,
	ET_PLANE2,
	ET_POLLYWOG,

	ET_FALLING,
};


}// namespace

#endif
