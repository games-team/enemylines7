#ifndef __font_ogl_h
#define __font_ogl_h
#include <string>
#include "coordinate.h"

typedef enum E_FontAnchor {
	FA_CENTER,
	FA_NW,
};


class Font_ogl {
	static char get_field(int letter,int x,int y);
public:

	static unsigned int dx();
	static unsigned int dy();

	static void drawletter_nodl(char letter,bool smooth=false);
	static void drawletter(char letter,bool smooth=false);


	static void genlist();
	static void write_nodl(const char *text,bool smooth=false,E_FontAnchor a=FA_NW);
	static void write(const char *text,bool smooth=false,E_FontAnchor a=FA_NW);

	static void write_nodl(const std::string str,bool smooth=false,E_FontAnchor a=FA_NW);
	static void write(const std::string str,bool smooth=false,E_FontAnchor a=FA_NW);

	static void write(C3 pos,const char *text,bool smooth=false,E_FontAnchor a=FA_NW);
	static void write(C3 pos,const std::string str,bool smooth=false,E_FontAnchor a=FA_NW);
};
#endif
