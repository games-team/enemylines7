
#include "coordinate.h"

class Position {
	C3f pos;
	C3f dir;
	C3f rot;

public:
	Position();
	virtual ~Position();



	void rotrans();
	void rotrans2();
	void rotate();
	void translate();
	void translate2();

	void move_forward(float mod);
	void move_backward(float mod);
	void move_left(float mod);
	void move_right(float mod);
	void move_rel_direction(C3f r,float ticks);
	void move_direction(C3f r,float ticks);

	C3f nextpos();

	C3f get_movement() const;
	void set_movement(C3f d);
	C3f get_position() const;
	void set_position(C3f p);
	C3f get_rotation() const;
	void set_rotation(C3f r);

	void movement_clear(C3);

	void cap_rot();
	void rotate(float x,float y);
};
