#include <sstream>
#include "SDL.h"
#include "SDL_opengl.h"
#include "SDL_image.h"

#include "tex.h"

namespace PRJID {

GLuint Tex::loadimg(const char *name,std::string dir) {
	SDL_Surface *image;
	std::ostringstream sstr;

	sstr << dir << name;

	image = IMG_Load(sstr.str().c_str());
	if (!image) {
		std::cerr << "   not found " << sstr.str() << std::endl;
		return 0;
	}

	GLuint r;
	glGenTextures(1,&r);
	glBindTexture(GL_TEXTURE_2D, r);  

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); 
	glTexImage2D(GL_TEXTURE_2D, 0, 3, image->w, image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);

   glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);


	std::cout << "   loaded " << name << "   " << image->w << " " << image->h <<"    " << r << std::endl;


	return r;
}
GLuint Tex::load(std::string filename,std::string path) {
	return loadimg(filename.c_str(),path);
}



} //namespace
