

#include "light.h"

#include "tweak/tweak.h"

namespace PRJID {

void Light::llight0(int codepos) {
	if (codepos!=Tweak::light0_codepos_i()) { return; }
	C3f lp;
	lp=Tweak::light0_position_C3f();
	GLfloat light0_pos[4]   = { lp.x,lp.y,lp.z,0 };
	C3f fc=Tweak::light0_color().tofloat()/256.0f;
	GLfloat light0_color[4]= {fc.x,fc.y,fc.z, 1.0f};	

	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_POSITION, light0_pos);
	glLightfv(GL_LIGHT0, GL_DIFFUSE,  light0_color);
}
void Light::llight1(int codepos) {
	if (codepos!=Tweak::light1_codepos_i()) { return; }
	C3f lp;
	lp=Tweak::light1_position_C3f();
	GLfloat light1_pos[4]   = { lp.x,lp.y,lp.z,0 };
	C3f fc=Tweak::light1_color().tofloat()/256.0f;
	GLfloat light1_color[4]= {fc.x,fc.y,fc.z, 1.0f};	

	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_pos);
	glLightfv(GL_LIGHT1, GL_DIFFUSE,  light1_color);
}
void Light::llight2(int codepos) {
	if (codepos!=Tweak::light2_codepos_i()) { return; }
	C3f lp;
	lp=Tweak::light2_position_C3f();
	GLfloat light2_pos[4]   = { lp.x,lp.y,lp.z,0 };
	C3f fc=Tweak::light2_color().tofloat()/256.0f;
	GLfloat light2_color[4]= {fc.x,fc.y,fc.z, 1.0f};	

	glEnable(GL_LIGHT2);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_pos);
	glLightfv(GL_LIGHT2, GL_DIFFUSE,  light2_color);
}


void Light::start_frame() {
    glDisable(GL_LIGHT0);
    glDisable(GL_LIGHT1);
    glDisable(GL_LIGHT2);
    glEnable(GL_LIGHTING);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
}

void Light::light(int codepos) {

	llight0(codepos);
	llight1(codepos);
	llight2(codepos);
}
void Light::fog() {
	C3f fc=Tweak::fog_color().tofloat()/256.0f;

	GLfloat fogColor[4]= {fc.x,fc.y,fc.z, 1.0f};	
	glFogi(GL_FOG_MODE, GL_LINEAR);
	glFogfv(GL_FOG_COLOR, fogColor);	
	glFogf(GL_FOG_DENSITY, Tweak::fog_density_f());
	glHint(GL_FOG_HINT, GL_NICEST);
	glFogf(GL_FOG_START, Tweak::fog_start_f());
	glFogf(GL_FOG_END, Tweak::fog_end_f());
	glEnable(GL_FOG);	
}

} //namespace
